package com.java4s.app;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBoot {
	private static final Logger logger = LoggerFactory.getLogger(SpringBoot.class);
	
      public static void main(String[] args)      {
         SpringApplication.run(SpringBoot.class, args);
     }


}
