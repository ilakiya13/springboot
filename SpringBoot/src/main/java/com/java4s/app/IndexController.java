package com.java4s.app;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
	private static final Logger logger = LoggerFactory.getLogger("");
	@RequestMapping("/")
	public String home(Map<String,Object>model){
		model.put("message","Hlo");
		return "index";
	}
	@RequestMapping("/next")
	public String next(Map<String,Object>model){
		model.put("message","Hlo");
		logger.error("Message logged at ERROR level");
	    logger.warn("Message logged at WARN level");
	    logger.info("Message logged at INFO level");

		return "next";
	}

}
